import sqlite3



class SuperFlashcardTool():
    def __init__(self, path_to_flashcards_db):
        self.con = sqlite3.connect(path_to_flashcards_db)
        self.cur = self.con.cursor()

    def table_names(self):
        query = self.cur.execute("SELECT name FROM sqlite_master WHERE type='table'")
        return query.fetchall()
    
    def table_records(self, table_name, **kwargs):
        query = self.cur.execute(f"SELECT * FROM {table_name}")
        return query.fetchall()

    def column_names(self, table_name):
        query = self.cur.execute(f"PRAGMA table_info({table_name})")
        return query.fetchall()
    
    def flashcards(self, user_name, deck_name, ):
        
        deck_id = self.cur.execute(f"SELECT Id FROM Deck WHERE title = '{deck_name}'").fetchone()[0]
        
        flashcards = self.cur.execute(f"SELECT back, front FROM Card WHERE deck={deck_id}").fetchall()
        
        flashcards = list(map(lambda x: (x[0].replace("<sound/>",""), x[1].replace("<sound/>","")) ,flashcards))
        
        
        return flashcards
    
    @staticmethod
    def convert_to_csv(flashcards, file_name="example.csv"):
        
        with open(file_name, "w", encoding="utf-8") as h:
            for front, back in flashcards:
                h.write(f"{front}, {back} \n")
    
    def close_connection(self):
        self.con.close()
        
    
# for row in cur.execute("SELECT back, front FROM Card WHERE deck=1"):
#     print(row)    


if __name__ == "__main__":
    
    path_to_flashcards_db = r"../2021_10_03_20_06.flashcard"
    
    tool = SuperFlashcardTool(path_to_flashcards_db)
    
    # print(tool.table_names())
    # print(tool.table_records("Deck"))
    # print(tool.column_names("Deck"))
    
    b = tool.flashcards("rzadki22", "my first english deck")
    tool.convert_to_csv(b)
    
    print(b)
    # print(tool.column_names("Card"))
    
    # tool.test()
    
    tool.close_connection()
